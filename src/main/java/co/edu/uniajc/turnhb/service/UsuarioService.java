package co.edu.uniajc.turnhb.service;

import co.edu.uniajc.turnhb.model.usuarioModel;
import co.edu.uniajc.turnhb.repository.UsuarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.Optional;

@Service
public class UsuarioService {
    private final UsuarioRepository usuarioRepository;
    @Autowired
    public UsuarioService(UsuarioRepository usuarioRepository) {
        this.usuarioRepository = usuarioRepository;
    }

    public usuarioModel getById(int id) {
        return usuarioRepository.getById(id);
    }

    @Query(nativeQuery = true, value = "SELECT" + "id" + ", celular" + ", nombre" + 
    		", apellido" + ",idrol" + "FROM Usuario" + " WHERE idrol =: idrol")
    
    public List<usuarioModel> findEstado(Integer estado) {
        return usuarioRepository.findEstado(estado);
    }

    public List<usuarioModel> findAll() {
        return usuarioRepository.findAll();
    }

    public List<usuarioModel> findAll(Sort sort) {
        return usuarioRepository.findAll(sort);
    }

    public <S extends usuarioModel> S save(S entity) {
        return usuarioRepository.save(entity);
    }

    public Optional<usuarioModel> findById(Long aLong) {
        return usuarioRepository.findById(aLong);
    }

    public boolean existsById(Long aLong) {
        return usuarioRepository.existsById(aLong);
    }

    public long count() {
        return usuarioRepository.count();
    }

    public void deleteById(Long aLong) {
        usuarioRepository.deleteById(aLong);
    }
    public usuarioModel update(usuarioModel usuarioModel){
        if(existsById(usuarioModel.getId()) == true)
        {
            usuarioRepository.saveAndFlush(usuarioModel);
        }
        return usuarioModel;
    }
}



