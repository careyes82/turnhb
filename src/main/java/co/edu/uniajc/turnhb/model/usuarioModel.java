package co.edu.uniajc.turnhb.model;

//import lombok.Data;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="Usuario")
public class usuarioModel implements Serializable{
	
	private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id")
    private long id;
    
    @Column(name="celular")
    private long celular;
    
    @Column(name="nombre")
    private String nombre;
    
    @Column(name="apellido")
    private String apellido;
    
    public usuarioModel() {
    	//Constructor
    }

    public usuarioModel(long id, long celular, String nombre, String apellido) {
        this.id = id;
        this.celular = celular;
        this.nombre = nombre;
        this.apellido = apellido;
    }

    public long getId() { return id; }

    public void setId(long id) { this.id = id; }

    public long getCelular() { return celular; }

    public void setCelular(long celular) { this.celular = celular; }

    public String getNombre() { return nombre; }

    public void setNombre(String nombre) { this.nombre = nombre; }

    public String getApellido() { return apellido; }

    public void setApellido(String apellido) { this.apellido = apellido; }
    
}



